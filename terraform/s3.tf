resource "aws_s3_bucket" "s3" {
  bucket = "adam-yells-at-cloud"
  acl    = "public-read"
}

resource "aws_s3_bucket_policy" "s3" {
  bucket = aws_s3_bucket.s3.id

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::adam-yells-at-cloud/*"
        }
    ]
  }
  POLICY
}
