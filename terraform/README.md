# Resources used by the blog

## Non tf-managed
- R53 hosted zone: adam-yells-at-cloud.com
- ACM Certs
  - 'www.adam-yells-at-cloud.com'

## tf-managed
- S3 bucket
  - S3 IAM policy
- Cloudfront Distribution
- R53 Alias for CF dist

## future tf-managed
- local-exec to build jekyll & upload to s3

# References
- [S3 Cloudfront R53 configuration](https://rangle.io/blog/frontend-app-in-aws-with-terraform/)
- [tf cf distribution](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution#example-usage)
