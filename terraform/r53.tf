resource "aws_route53_record" "r53-www" {
  zone_id = "Z04811072EAEWLHHY02G1" #adam-yells-at-cloud.com
  name    = "www.adam-yells-at-cloud.com"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.s3_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.s3_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "r53-root" {
  zone_id = "Z04811072EAEWLHHY02G1" #adam-yells-at-cloud.com
  name    = "adam-yells-at-cloud.com"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.s3_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.s3_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}
