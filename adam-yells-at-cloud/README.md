# Docker based development
```
docker  run  -v `pwd`:`pwd` -w `pwd` -i -p 4000:4000 -t jekyll/jekyll:latest jekyll serve --watch --future
```
# Update \_site contents (this is also why it's in .gitignore)
```
jekyll build
```

# Live serve & reload
```
jekyll serve --watch --future
```

# Deploy command
```
aws s3 sync _site s3://adam-yells-at-cloud/ --delete
```
