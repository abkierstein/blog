---
layout: single
title:  "About Me"
date:   2021-01-08 16:44:58 -0800
categories: intro personal
---
My name is Adam Kierstein and I'm a SRE / devops / linux systems engineer by trade.

My objective for this blog to to document interesting/challenging things I have been working on and HOPEFULLY leave some pieces of information useful to others.
